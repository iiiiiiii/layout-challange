# Erply HTML/CSS challange

## Goal

Page is created only using HTML and CSS. No Javascript was used to create this page of provided design. This page should include all the elements visible in the Figma. Menu bar of the design is hidden, please do not use this on your page. End result should look like this:

![mockup.png](mockup.png "Screenshot 2023-01-02 at 23.08.48.png")

## Result

Solution is presentend in a three forms:
1. Souce files, located in ```src/``` folder.
2. Ready version with sensible embedding in ```ready/``` directory
3. Fully embedded single-file version in ```compiled.html```

Depending on point of view and context these versions might have different usecases. Fonts are not being cached in ```base64``` encoded string, so it might have an advantage of not having too much requests, but it has larger side encoded.


### ```src/index.html```
This solution tries to make everything as separate as possible using HTML/CSS only imports to make files smaller and design clear to understand. By looking at the folder structure it is understandable what page needs to operate and what components are available. More HTTP requests but much modular approach. Good starting point for porting into Javascript framework or Web Component's Custom Elements.

### ```ready/index.html```
Might be called a production build, that has all styles compiled together and fonts are being loaded from separate folder. Also icons sprite is embedded so one less request to the server and it does not ruin page load too much. This is how page have been developed during solving this challange.

### ```compiled.html```
Everything everybit all at once placed in single file. Has only one true request to server, styles, fonts and icons are placed inside one document mostly just for fun, however might be useful in some cases. Like when it is set what is critical CSS and what fonts should be loaded, so that there is no pesky jumping going on.

It is actually fastest way to render this pafe, takes only about 7 seconds to load everything in emulated Slow 3G (Chrome Network panel).

### How it compares to others solutions?
Production version takes about 9 seconds and source verson takes the longest, whopping 12 seconds to render.

